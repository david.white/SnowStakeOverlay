﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SnowStakeOverlay
{
    class ImageFileHandler
    {
        DateTime currentImageDownloadTime;
        Bitmap currentImage, staticImage;
        public string staticImageLocation, currentImageLocation;
        public ImageFileHandler()
        {
            currentImageDownloadTime = DateTime.MinValue;
            currentImage = null;
            staticImage = null;

            string appdatadir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            appdatadir = Path.Combine(appdatadir, "SnowStakeImages");
            if (!File.Exists(appdatadir))
            {
                System.IO.Directory.CreateDirectory(appdatadir);
            }

            staticImageLocation = Path.Combine(appdatadir, "staticSnowStake.jpg");
            currentImageLocation = Path.Combine(appdatadir, "currentSnowStake.jpg");
            loadCurrentImage();
            loadStaticImage();
        }

        public Bitmap GetStaticImage()
        {
            return staticImage;
        }

        public Bitmap GetCurrentSnowStakeImage()
        {
            return currentImage;
        }

        //These get called by the UI update callback to make sure the image is reloaded from the new file
        public void loadStaticImage()
        {

            try
            {
                //open the image this way so the file isn't locked
                using (var vmpTemp = new Bitmap(staticImageLocation))
                {
                    staticImage = new Bitmap(vmpTemp);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        //These get called by the UI update callback to make sure the image is reloaded from the new file
        public void loadCurrentImage()
        {
            try
            {
                using (var bmpTemp = new Bitmap(currentImageLocation))
                {
                    currentImage = new Bitmap(bmpTemp);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
      
        public void DownloadCurrentImage(Action<object, AsyncCompletedEventArgs> UIUpdateCallback)
        {
            WebClient wc = new WebClient();
            wc.DownloadFileCompleted += new AsyncCompletedEventHandler(UIUpdateCallback);
            wc.DownloadFileAsync(new Uri("http://cams.winterparkresort.com/snow-stake-cam.jpg"), currentImageLocation);
        }

        public void DownloadStaticImage(Action<object, AsyncCompletedEventArgs> asyncDownloadDelegate)
        {
            WebClient wc = new WebClient();
            wc.DownloadFileCompleted += new AsyncCompletedEventHandler(asyncDownloadDelegate);
            wc.DownloadFileAsync(new Uri("http://cams.winterparkresort.com/snow-stake-cam.jpg"), staticImageLocation);
        }
    }
}
