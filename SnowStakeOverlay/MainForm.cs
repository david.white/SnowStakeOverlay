﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SnowStakeOverlay
{
    public partial class MainForm : Form
    {
        ImageFileHandler ifh;
        public MainForm()
        {
            InitializeComponent();
            ifh = new ImageFileHandler();
            pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            UpdateImage(0);
        }

        private void GetImage_Click(object sender, EventArgs e)
        {
            ifh.DownloadCurrentImage(CurrentImageUpdateCallback);
        }

        private void downloadStaticButton_Click(object sender, EventArgs e)
        {
            ifh.DownloadStaticImage(StaticImageUpdateCallback);
        }

        private void CurrentImageUpdateCallback(object sender, AsyncCompletedEventArgs e)
        {
            //Tell the file handler to recreate the Bitmap
            ifh.loadCurrentImage();
            UpdateImage(0);
        }

        private void StaticImageUpdateCallback(object sender, AsyncCompletedEventArgs e)
        {
            //Tell the file handler to recreate the Bitmap
            ifh.loadStaticImage();
            UpdateImage(0);
        }

        private void hScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            if (e.Type == ScrollEventType.EndScroll)
            {
                UpdateImage(e.NewValue);
                this.opacityLabel.Text = e.NewValue.ToString();
            } 
        }

        private void UpdateImage(int overlayOpacity)
        {
            if (overlayOpacity > 100)
            {
                throw new ArgumentException("overlayOpacity over 100: " + overlayOpacity.ToString());
            }
            //Opacity setting code based on http://stackoverflow.com/questions/4779027/changing-the-opacity-of-a-bitmap-image
            ColorMatrix matrix = new ColorMatrix();
            ImageAttributes attributes = new ImageAttributes();
            //attributes.SetColorMatrix(matrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);

            Graphics gfx = pictureBox1.CreateGraphics();
            Bitmap overlay = null;
            try {
                //overlay = new Bitmap("C:\\Users\\david\\SnowStakeImages\\emptySnowStake.jpg");
                overlay = ifh.GetStaticImage();

                matrix.Matrix33 = (100f);
                attributes.SetColorMatrix(matrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
                gfx.DrawImage(ifh.GetCurrentSnowStakeImage(), new Rectangle(0, 0, overlay.Width, overlay.Height), 0, 0, overlay.Width, overlay.Height, GraphicsUnit.Pixel, attributes);

                matrix.Matrix33 = (overlayOpacity / 100f);
                attributes.SetColorMatrix(matrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
                gfx.DrawImage(overlay, new Rectangle(0, 0, overlay.Width, overlay.Height), 0, 0, overlay.Width, overlay.Height, GraphicsUnit.Pixel, attributes);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            pictureBox1.Update();
        }

    }
}
